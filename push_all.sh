#!/bin/bash
# This script fetches all changes from AUR (suitable for co-maintained packages)

set -e

parallel aurpublish ::: */
git push
